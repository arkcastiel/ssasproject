<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aplikasi;
use App\Bank;
use App\Database;
use App\User;
use Auth;

class DatabaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listdb = Database::all();
        return view('database.index', compact('listdb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listbank = Bank::all();
        return view('database.create', compact('listbank'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sistemoperasi' => 'required',
            'nama' => 'required',
            'jenis' => 'required',
            'ip' => 'required',
            'port' => 'required',
            'bank_id' => 'required'
        ]);

        $user_id = Auth::user()->id;

        $database = Database::create([
            'sistemoperasi' => $request->sistemoperasi,
            'nama' => $request->nama,
            'jenis' => $request->jenis,
            'ip' => $request->ip,
            'port' => $request->port,
            'bank_id' => $request->bank_id,
            'user_id' => $user_id

        ]);

        return redirect('/database');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $database = Database::findorfail($id);
        $listaplikasi = Aplikasi::all();
        $listbank = Bank::all();
        return view('database.edit', compact('database','listbank', 'listaplikasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'sistemoperasi' => 'required',
            'nama' => 'required',
            'jenis' => 'required',
            'ip' => 'required',
            'port' => 'required',
            'bank_id' => 'required'
        ]);

        $user_id = Auth::user()->id;
        $database = Database::findorfail($id);

        $database_data = [
            'sistemoperasi' => $request->sistemoperasi,
            'nama' => $request->nama,
            'jenis' => $request->jenis,
            'ip' => $request->ip,
            'port' => $request->port,
            'bank_id' => $request->bank_id,
            'user_id' => $user_id
        ];

      $database->update($database_data);
      return redirect('/database');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
