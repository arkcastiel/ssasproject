<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index');
Route::get('/dashboard-faq', 'DashboardController@faq');
Route::get('/dashboard-dokumen', 'DashboardController@dokumen');
Route::get('/dashboard-db', 'DashboardController@db');

Route::get('/blank', function () {
    return view('blank');
});

// Route::resource('bank', 'BankController');
// Route::resource('aplikasi', 'AplikasiController');

Route::group(['middleware' => 'auth'], function () {

    Route::resource('bank', 'BankController');
    Route::resource('aplikasi', 'AplikasiController');
    Route::resource('database', 'DatabaseController');
    Route::resource('faq', 'FaqController');
    Route::resource('dokumentasi', 'DokumentasiController');

    // //Profile
    // Route::resource('profile', 'ProfileController')->only(['index','update','store']);
    // // Post
    // Route::resource('post', 'PostController');
    // Route::post('postlike/{post_id}', 'PostController@like')->name('komentar.like');

    // // Komentar
    // Route::get('komentar/create/{post_id}', 'KomentarController@create')->name('komentar.create');
    // Route::get('komentar/{post_id}/edit', 'KomentarController@edit')->name('komentar.edit');
    // Route::post('komentar/store', 'KomentarController@store')->name('komentar.store');
    // Route::delete('/komentar/{komentar_id}', 'KomentarController@destroy');

    // Route::put('komentar/update', 'KomentarController@update')->name('komentar.update');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
