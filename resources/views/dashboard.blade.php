@extends('layouts.master')
@section('content')
 <!-- Content Header (Page header) -->
 <section class="content-header">
  <h1>
    DASHBOARD
    <small>SSAS PROJECT</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>

</section>

<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{$totaluser}}</h3>
          <p>Total User</p>
        </div>
        <div class="icon">
          <i class="fa fa-user"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{$totalaplikasi}}</h3>
          <p>Total Aplikasi</p>
        </div>
        <div class="icon">
          <i class="fa fa-server"></i>
        </div>
        <a href="/aplikasi" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{$totalbank}}</h3>
          <p>Total Bank</p>
        </div>
        <div class="icon">
          <i class="fa fa-institution"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{$totaldb}}</h3>
          <p>Total Database</p>
        </div>
        <div class="icon">
          <i class="fa fa-database"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    </div>
  <!-- /.row -->
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Aplikasi</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-bordered" id="table_aplikasi">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Bank</th>
                <th>Jenis Aplikasi</th>
                <th>Nama Aplikasi</th>
                <th>Nama War Aplikasi</th>
                <th>IP:PORT</th>
                <th scope="col" style="">Actions</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($listaplikasi as $key=>$value)
              <tr>
                  <td>{{$value->id}}</th>
                  <td>{{$value->bank->nama}}</td>
                  <td>{{$value->jenis}}</td>
                  <td>{{$value->nama}}</td>
                  <td>{{$value->war}}</td>
                  <td>{{$value->ip.":".$value->port}}</td>
                  <td>
                      <a target="_blank" href="http://{{$value->ip.":".$value->port.$value->war}}" class="btn btn-sm btn-primary"> <i class="fa fa-plane"></i> GO</a>
                  </td>
              </tr>
              @empty
              @endforelse
          </tbody>
        </table>
        </div>
      </div>
      <!-- /.box -->
    </div>

  </div>
  <!-- /.row (main row) -->
</section>
<!-- /.content -->

@endsection

@push('scripts_body')
<script>
    $('#table_aplikasi').DataTable();
</script>
@endpush

