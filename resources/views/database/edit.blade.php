@extends('layouts.master')

@section('content')
<br>
<div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Edit Database</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered">

          <div class="card shadow mb-4">
            <!-- Card Body -->
            <div class="card-body">
                <form action="/database/{{$database->id}}" method="POST" role="form" class="form-horizontals">
                @csrf
                @method('put')
                    <div class="form-group">
                    <label for="bank_id">Bank :    </label>
                    <select class="form-control" name="bank_id" id="bank_id">
                        <option value="{{$database->bank->id}}">{{$database->bank->nama}}</option>
                        @foreach ($listbank as $item)
                             <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                    @error('bank_id')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="sistemoperasi">Sistem Operasi Database :</label>
                    <select class="form-control" name="sistemoperasi" id="sistemoperasi">
                        <option value="{{$database->sistemoperasi}}">{{$database->sistemoperasi}}</option>
                        <option value="Linux">Linux</option>
                        <option value="AIX">IBM AIX</option>
                    </select>
                    @error('sistemoperasi')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama Database</label>
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="{{$database->nama}}">
                    @error('nama')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </div>
                    @enderror
                    <small id="emailHelp" class="form-text text-muted">Contoh : CARD125, ATM133, BE_1373, FESMT</small>
                  </div>
                  <div class="form-group">
                    <label for="jenis">Jenis Database</label>
                    <select class="form-control" name="jenis" id="jenis">
                        <option value="{{$database->jenis}}">{{$database->jenis}}</option>
                        <option value="DB2">DB2</option>
                        <option value="POSTGRES">Postgres</option>
                        <option value="SYBASE">SYBASE</option>
                    </select>
                    @error('jenis')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </div>
                    @enderror
                    <small id="emailHelp" class="form-text text-muted">Contoh : Aplikasi Switching</small>
                  </div>
                  <div class="form-group">
                    <label for="ip">IP Database</label>
                    <input type="text" class="form-control" name="ip" id="ip" placeholder="{{$database->ip}}">
                    @error('ip')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </div>
                    @enderror
                    <small id="emailHelp" class="form-text text-muted">Contoh : 192.168.1.xxx</small>
                  </div>
                  <div class="form-group">
                    <label for="port">Port Database</label>
                    <input type="text" class="form-control" name="port" id="port" placeholder="{{$database->port}}">
                    @error('port')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </div>
                    @enderror
                    <small id="emailHelp" class="form-text text-muted">Contoh : 50003</small>
                  </div>
                   </div>
                  <button type="submit" class="btn btn-primary mb-3">Edit</a>
                </form>
            </div>
        </div>
      </table>

      </div>
    </div>
</div>
@endsection
