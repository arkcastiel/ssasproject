@extends('layouts.master')

@section('content')
<section class="content-header">
  <br>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">FAQ</li>
  </ol>
  <hr>
  {{-- <nav aria-label="Page navigation example">
    <ul class="pagination">
      <li class="page-item">
        <a class="page-link" href="#" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <li class="page-item"><a class="page-link" href="">1</a></li>
      <li class="page-item"><a class="page-link" href="">2</a></li>
      <li class="page-item"><a class="page-link" href="">3</a></li>
      <li class="page-item">
        <a class="page-link" href="#" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav> --}}
</section>

<div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Daftar Dokumentasi</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered">
          <thead>
            <a href="/dokumentasi/create" class="btn btn-primary mb-3">Tambah</a>
            <br>
            <br>
            <tr>
              <th style="width: 10px">ID</th>
              <th>Judul</th>
              <th>Isi</th>
              <th scope="col" style="">Actions</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($listdokumen as $value)
            <tr>
                <td width="3%">{{$value->id}}</th>
                <td width="30%">{{$value->judul}}</td>
                <td width="63%">{!!$value->isi!!}</td>
                <td width="4">
                    <form action="/dokumentasi/{{$value->id}}" method="POST">
                        <a href="/dokumentasi/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>
        @endforelse
        {{ $listdokumen->links() }}
        </tbody>
      </table>
      </div>
    </div>
</div>
@endsection
