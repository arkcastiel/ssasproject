@extends('layouts.master')

@section('content')
<section class="content-header">
  <br>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Bank</li>
  </ol>
  <hr>
  {{-- <nav aria-label="Page navigation example">
    <ul class="pagination">
      <li class="page-item">
        <a class="page-link" href="#" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <li class="page-item"><a class="page-link" href="">1</a></li>
      <li class="page-item"><a class="page-link" href="">2</a></li>
      <li class="page-item"><a class="page-link" href="">3</a></li>
      <li class="page-item">
        <a class="page-link" href="#" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav> --}}
</section>

<div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Daftar Bank</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="width: 10px">ID</th>
              <th>Nama Bank</th>
              <th scope="col" style="">Actions</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($listbank as $key=>$value)
            <tr>
                <td>{{$value->id}}</th>
                <td>{{$value->nama}}</td>
                <td>
                    <form action="/bank/{{$value->id}}" method="POST">
                        <a href="/bank/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                    </form> 
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>             
        @endforelse
        {{ $listbank->links() }}         
        </tbody>
      </table>
      <br>
      <br>      

      <a href="/bank/create" class="btn btn-primary mb-3">Tambah</a>
      </div>
    </div>
</div>
@endsection