@extends('layouts.master')

@section('content')
<section class="content-header">
  <h1>
    DASHBOARD
    <small>SSAS PROJECT</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Edit Nama Bank</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered">

          <div class="card shadow mb-4">
            <!-- Card Body -->
            <div class="card-body">
                <form action="/bank/{{$bank->id}}" method="POST" role="form" class="form-horizontals">
                @csrf
                @method('put')
                  <div class="form-group">
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="{{$bank->nama}}">
                    @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                    <small id="emailHelp" class="form-text text-muted">Contoh : Bank Aceh Syariah</small>
                  </div>
                  <button type="submit" class="btn btn-primary mb-3">Edit</a>
                </form>
            </div>
        </div>
      </table>

      </div>
    </div>
</div>
@endsection
