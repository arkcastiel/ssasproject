<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      @auth
      <div class="user-panel">
        {{-- <div class="pull-left image">
          <img src="{{ asset('adminlte-v2/') }}/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div> --}}

        <div class="sidebar-menu" data-widget="tree">
          <li class="">
            <a href="#"><i class="fa fa-user"></i><span> Welcome {{ Auth::user()->name }}</span></a>
            {{-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> --}}
          </li>
        </div>
      </div>
      @endauth
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      @guest
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">User</li>
        <li class="">
          <a href="/login">
            <i class="fa fa-sign-in"></i> <span>Login</span>
          </a>
        </li>
        <li class="">
            <a href="/register">
              <i class="fa fa-registered "></i> <span>Register</span>
            </a>
          </li>
        </ul>
      </ul>
      @endguest

      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="">
          <a href="/dashboard">
            <i class="fa fa-laptop"></i> <span>Aplikasi</span>
          </a>
        </li>
        <li class="">
            <a href="/dashboard-db">
              <i class="fa fa-database"></i>
              <span>Database</span>
            </a>
          </li>
        <li class="">
          <a href="/dashboard-faq">
            <i class="fa fa-question-circle"></i>
            <span>FAQ</span>
          </a>
        </li>
        <li class="">
            <a href="/dashboard-dokumen">
              <i class="fa fa-files-o"></i>
              <span>Dokumentasi</span>
            </a>
          </li>
      </ul>

      @auth
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">ADMIN PAGE</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Menu Entri</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/bank"><i class="fa fa-bank"></i> Entri Bank</a></li>
            <li><a href="/aplikasi"><i class="fa fa-laptop"></i> Entri Aplikasi</a></li>
            <li><a href="/database"><i class="fa fa-server"></i> Entri Database</a></li>
            <li><a href="/faq"><i class="fa fa-question-circle"></i> Entri FAQ</a></li>
            <li><a href="/dokumentasi"><i class="fa fa-files-o"></i> Entri Dokumentasi</a></li>
            {{-- <li><a href="pages/forms/editors.html"><i class="fa fa-user"></i> Pemeliharaan User</a></li> --}}
          </ul>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">LOGOUT</li>
        <li class="nav-item" href="{{ route('logout') }}">
          <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
              <i class="nav-icon fa fa-sign-out"></i>
            <span>Sign out</span></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
       </li>
      </ul>
      @endauth
    </section>

    <!-- /.sidebar -->
  </aside>
