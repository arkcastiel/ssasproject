<header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SSAS</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SSAS</b> Project</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          @guest
          <li class="nav-item active">
            <a class="nav-link" href="/login">Login</a>
          </li>
          @endguest
          @auth
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="#" class="" alt=""><i class="fa fa-user"></i>
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <br>
                <h1><b>{{ Auth::user()->name }}</b></h1> 
                <small><i>Member since Nov. 2013</i></small>
                </p>
          </li>              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  {{-- <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Sign out</a> --}}
                  <a href="" class="btn btn-default btn-flat" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    {{-- <i class="nav-icon fa fa-sign-out"></i> --}}
                  <span>Sign out</span></a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                </div>
              </li>
              @endauth
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>