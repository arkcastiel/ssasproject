@extends('layouts.master')
@section('content')
 <!-- Content Header (Page header) -->
 <section class="content-header">
    <h1>
      DASHBOARD
      <small>Dokumentasi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard Dokuemntasi</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- /.row -->
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Dokumentasi</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">ID</th>
                <th>Pertanyaan</th>
                <th>Jawaban</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($listdok as $value)
              <tr>
                  <td>{{$value->id}}</th>
                  <td>{{$value->judul}}</td>
                  <td>{{$value->isi}}</td>
              </tr>
          @empty
              <tr colspan="3">
                  <td>No data</td>
              </tr>
          @endforelse
          {{ $listdok->links() }}
          </tbody>
        </table>
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
  <!-- /.row (main row) -->
</section>
<!-- /.content -->
@endsection


