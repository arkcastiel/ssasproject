@extends('layouts.master')
@section('content')
 <!-- Content Header (Page header) -->
 <section class="content-header">
    <h1>
      DASHBOARD
      <small>Database</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Daftar Database</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- /.row -->
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Database</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Bank</th>
                <th>Sistem Operasi</th>
                <th>Jenis Database</th>
                <th>Nama Database</th>
                <th>IP:PORT</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($listdb as $key=>$value)
              <tr>
                  <td>{{$value->id}}</th>
                  <td>{{$value->bank->nama}}</td>
                  <td>{{$value->sistemoperasi}}</td>
                  <td>{{$value->jenis}}</td>
                  <td>{{$value->nama}}</td>
                  <td>{{$value->ip.":".$value->port}}</td>
              </tr>
              @empty
                  <tr colspan="6">
                      <td>No data</td>
                  </tr>
              @endforelse
          </tbody>
        </table>
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
  <!-- /.row (main row) -->
</section>
<!-- /.content -->
@endsection
