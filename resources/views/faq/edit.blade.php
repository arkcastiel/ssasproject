@extends('layouts.master')

@section('content')
<section class="content-header">
  <h1>
    DASHBOARD
    <small>SSAS PROJECT</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Edit FAQ</li>
  </ol>
</section>

<div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Edit FAQ</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered">

          <div class="card shadow mb-4">
            <!-- Card Body -->
            <div class="card-body">
                <form action="/faq/{{$faq->id}}" method="POST" role="form" class="form-horizontals">
                @csrf
                @method('put')
                  <div class="form-group">
                    <label for="judul">Pertanyaan</label>
                    <input type="text" class="form-control" name="judul" id="judul" value="{{$faq->judul}}">
                    @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                    {{-- <small id="emailHelp" class="form-text text-muted"></small> --}}
                  </div>
                  <div class="form-group">
                    <label for="judul">Jawaban</label>
                    <textarea class="form-control" name="isi" id="isi"></textarea>
                    @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                    {{-- <small id="emailHelp" class="form-text text-muted"></small> --}}
                  </div>
                  <button type="submit" class="btn btn-primary mb-3">Edit</a>
                </form>
            </div>
        </div>
      </table>
      </div>
    </div>
</div>
@push('script_ckeditor')
<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script>
   var isi = document.getElementById("isi");
     CKEDITOR.replace(isi,{
     language:'en-gb'
   });
   CKEDITOR.config.allowedContent = true;
//    CKEDITOR.config.enterMode      = CKEDITOR.ENTER_BR;
//    CKEDITOR.config.shiftEnterMode = CKEDITOR.ENTER_P;
</script>
@endpush
@endsection

