@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <section class="content-header">
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Bank</li>
  </ol>
  <br>
  <hr>
</section>
<div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Daftar Aplikasi</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <a href="/aplikasi/create" class="btn btn-primary mb-3">Tambah</a>
        <br>
        <br>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Bank</th>
              <th>Jenis Aplikasi</th>
              <th>Nama Aplikasi</th>
              <th>Nama War Aplikasi</th>
              <th>IP:PORT</th>
              <th scope="col" style="">Actions</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($listaplikasi as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->bank->nama}}</td>
                <td>{{$value->jenis}}</td>
                <td>{{$value->nama}}</td>
                <td>{{$value->war}}</td>
                <td>{{$value->ip.":".$value->port}}</td>
                <td>
                    <form action="/aplikasi/{{$value->id}}" method="POST">
                        <a href="/aplikasi/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                    </form> 
                </td>
            </tr>
            @empty
                <tr colspan="6">
                    <td>No data</td>
                </tr>  
            @endforelse        
        </tbody>
      </table>
      </div>
    </div>
</div>
@endsection