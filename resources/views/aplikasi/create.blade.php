@extends('layouts.master')

@section('content')
<br>
<div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Input Jenis Aplikasi</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered">

          <div class="card shadow mb-4">
            <!-- Card Body -->
            <div class="card-body">
                <form action="/aplikasi" method="POST" role="form" class="form-horizontals">
                @csrf
                    <div class="form-group">
                    <label for="bank_id">Bank :    </label>
                    <select class="form-control" name="bank_id" id="bank_id">
                        <option value="">Pilih Bank</option>
                        @foreach ($listbank as $item)
                             <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                    @error('bank_id')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="jenis">Jenis Aplikasi</label>
                    <select class="form-control" name="jenis" id="jenis">
                        <option value="">Pilih Aplikasi</option>
                        <option value="CBS">Aplikasi Core Banking System</option>
                        <option value="Switching">Aplikasi Switching</option>
                        <option value="Surrounding">Aplikasi Surrounding</option>
                    </select>
                    @error('jenis')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </div>
                    @enderror
                    <small id="emailHelp" class="form-text text-muted">Contoh : Aplikasi Switching</small>
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama Aplikasi</label>
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Aplikasi...">
                    @error('nama')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </div>
                    @enderror
                    <small id="emailHelp" class="form-text text-muted">Contoh : ereport, ecardman, BE, FE</small>
                  </div>
                  <div class="form-group">
                    <label for="war">Nama War Aplikasi</label>
                    <input type="text" class="form-control" name="war" id="war" placeholder="Nama War Aplikasi...">
                    @error('war')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </div>
                    @enderror
                    <small id="emailHelp" class="form-text text-muted">Catatan : nama war harus diawali dengan "/". Dan jika war ROOT.war maka nama war adalah /</small>
                  </div>
                  <div class="form-group">
                    <label for="ip">IP Aplikasi</label>
                    <input type="text" class="form-control" name="ip" id="ip" placeholder="IP Aplikasi Server Backup...">
                    @error('ip')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </div>
                    @enderror
                    <small id="emailHelp" class="form-text text-muted">Contoh : 192.168.1.xxx</small>
                  </div>
                  <div class="form-group">
                    <label for="port">Port Aplikasi</label>
                    <input type="text" class="form-control" name="port" id="port" placeholder="Port Server Aplikasi...">
                    @error('port')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </div>
                    @enderror
                    <small id="emailHelp" class="form-text text-muted">Contoh : 6060</small>
                  </div>
                   </div>
                  <button type="submit" class="btn btn-primary mb-3">Tambah</a>
                </form>
            </div>
        </div>
      </table>
      </div>
    </div>
</div>
@endsection
